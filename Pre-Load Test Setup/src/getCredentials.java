

import org.openqa.selenium.WebDriver;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;

/**
 * @author aoganezov
 */

public class getCredentials {
	public static String  username; //read in username and password
    public static String passwrd;
	public static String targetDir;
	
	/** PICK AN ENVIRONMENT:
	 * Choose which you want to test in, by uncommenting it, and commenting out the other 2.
	 */
	public static String url = "https://staging.juralaw.us/";
	//public static String url = "https://www.juralaw.net/";
	//public static String url = "https://dev.juralaw.net/";

	public static String getDir(String url) {
		if (url == "https://staging.juralaw.us/") {
			targetDir =  "G:/AUTOMATED SOFTWARE TESTING/Automation JuraLaw Property Files/staging";
			return targetDir;
		} else if (url == "https://www.juralaw.net/") {
			targetDir =  "G:/AUTOMATED SOFTWARE TESTING/Automation JuraLaw Property Files/staging";
			return targetDir;
		} else if (url == "https://dev.juralaw.net/") {
			targetDir =  "G:/AUTOMATED SOFTWARE TESTING/Automation JuraLaw Property Files/dev";
			return targetDir;
		} else {
			return null;
		}
		
	}
	
	public static String getLogin(String dirFromGetDirMethod) throws IOException {
		File dir = new File(dirFromGetDirMethod);
        File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains("username=")) return line.substring(9);
                    }
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
        return "NO USERNAME";
       }
	
	public static String getPassword(String dirFromGetDirMethod) throws IOException {
		File dir = new File(dirFromGetDirMethod);
        File[] files = dir.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                BufferedReader inputStream = null;
                try {
                    inputStream = new BufferedReader(new FileReader(f));
                    String line;
                    while ((line = inputStream.readLine()) != null) {
                    	if (line.contains("password=")) return line.substring(9);
                    }
                } catch (IOException e){
                    System.out.println("createDirectory failed:" + e);
                } finally {
                    if (inputStream != null) inputStream.close();
                }
            }
        }
        return "NO PASSWORD";
       }
		WebDriver driver;
		int saving_tries = 1;

}
