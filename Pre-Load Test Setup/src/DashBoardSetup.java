import java.net.MalformedURLException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
//import org.testng.annotations.Test;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.*;

import com.lawbulletin.sqa.juralawlibrary.DashboardPage;
import com.lawbulletin.sqa.juralawlibrary.LoginPage;
import com.lawbulletin.sqa.juralawlibrary.LogoutPage;
import com.lawbulletin.sqa.seleniumwebdriver.SeleniumRemoteWebDriver;

public class DashBoardSetup {
	
		WebDriver driver = new FirefoxDriver();
		LogoutPage logoutPage = new LogoutPage(driver);
	
	
	@Test
	public void dashboardPortlets() throws MalformedURLException, Exception{
		
//		RemoteWebDriver driver = SeleniumRemoteWebDriver.getNewRemoteWebDriverInstance("firefox","http://172.16.2.60:4444/wd/hub" );
		
		LoginPage loginPage = new LoginPage(driver);
		DashboardPage dashBoard = new DashboardPage(driver);
	
		String url = getCredentials.url;
        String dir = getCredentials.getDir(url);
        driver.get(url);
        String login = getCredentials.getLogin(dir);
        String password = getCredentials.getPassword(dir);
        loginPage.loginToJuraLaw(login, password);
        System.out.println("User " +login+ " signed into " +url+ "\n");
//		dashBoard.addCourtCallStatusPortlet();
//		Set<Cookie> cookieList;
//		Cookie JSession = driver.manage().getCookieNamed("LR_JSESSIONID");
//		String sessionCookie = driver.manage().getCookieNamed("LR_JSESSIONID").toString();
//		
//		Cookie crsf = driver.manage().getCookieNamed("CSRF_HEADER");
//		String crsfCookie = driver.manage().getCookieNamed("CSRF_HEADER").toString();
////		for (Cookie getcookies : cookieList){
////			
////			System.out.println(getcookies);
////		}
//		System.out.println(sessionCookie);
//		System.out.println(crsfCookie);
//		
//		driver.manage().addCookie(JSession);
//		driver.manage().addCookie(crsf);
////		driver.manage().addCookie((Cookie) driver.manage().getCookies());
//	     String  sessionID= ((RemoteWebDriver) driver).getSessionId().toString();
//	      
//	     System.out.print(sessionID);
//		wait(2);
	     
//	     driver.get("https://www.staging.juralaw.us/c/portal/update_layout;jsessionid=" + sessionID);
	     
//	     driver.get("https://www.staging.juralaw.us/c/portal/update_layout;jsessionid=" + sessionCookie + "?cmd=add&currentURL=%2Fuser%2Fjuralawtest-docketmgr%2Fhome%3Bjsessionid%3D"+ sessionCookie +"&dataType=json&doAsUserId=&p_auth=JuvGRwM9&p_l_id=63952&p_p_col_id=column-1&p_p_col_pos=0&p_p_i_id=&p_p_id=courtCallStatus_WAR_lbportal&p_p_isolated=1&p_v_g_id=12977");
//		https://www.staging.juralaw.us/c/portal/update_layout;jsessionid=86390A002106A74F9F62832B8FBE4654?cmd=add&currentURL=%2Fuser%2Fjuralawtest-docketmgr%2Fhome%3Bjsessionid%3D86390A002106A74F9F62832B8FBE4654&dataType=json&doAsUserId=&p_auth=grgSZ7JV&p_l_id=63952&p_p_col_id=column-1&p_p_col_pos=0&p_p_i_id=&p_p_id=courtCallStatus_WAR_lbportal&p_p_isolated=1&p_v_g_id=12977
	     
//	     Actions actions = new Actions(driver);
//	     WebElement menuHoverLink = driver.findElement(By.xpath(".//*[@id='_145_addContent']/a"));
//	     actions.moveToElement(menuHoverLink).clickAndHold();
//	     
//	     WebElement subLink = driver.findElement(By.cssSelector("a[data-portlet-id='courtCallStatus_WAR_lbportal']"));
//	     actions.moveToElement(subLink);
//	     actions.click();
//	     actions.perform();
	     
	     
	     
//	     driver.findElement(By.xpath("//a[@href='javascript:;'][1]")).click();
//	     driver.findElement(By.xpath("//a[@href='javascript:;'][1]")).click();
//	     
//		if(driver.findElement(By.cssSelector("a[data-portlet-id='courtCallStatus_WAR_lbportal']")).isEnabled()){
//			
//			driver.findElement(By.cssSelector("a[data-portlet-id='courtCallStatus_WAR_lbportal']")).click();
//		}
	
		//checking to see if Portlet is already open.. If not, we open it.
        try {
			driver.findElement(By.id("portlet_courtCallStatus_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addCourtCallStatusPortlet();
			System.out.println("Court Status Portlet added.");
			dashBoard.waitForThisManySeconds(1);
		}
			
		try {
			driver.findElement(By.id("portlet_courtCallVerification_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addCourtCallVerification();
			System.out.println("Court Call Verification added.");
			dashBoard.waitForThisManySeconds(1);
		}
		
		try {
			driver.findElement(By.id("portlet_courtRuleUpdates_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addCourtRuleUpdates();
			System.out.println("Court Rules added.");
			dashBoard.waitForThisManySeconds(1);
		}
		
		try {
			driver.findElement(By.id("portlet_nyCourtCallStatus_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addCourtCalendars();//https://www.staging.juralaw.us/c/portal/update_layout;jsessionid=86390A002106A74F9F62832B8FBE4654?cmd=add&currentURL=%2Fuser%2Fjuralawtest-docketmgr%2Fhome%3Bjsessionid%3D86390A002106A74F9F62832B8FBE4654&dataType=json&doAsUserId=&p_auth=grgSZ7JV&p_l_id=63952&p_p_col_id=column-1&p_p_col_pos=0&p_p_i_id=&p_p_id=courtCallStatus_WAR_lbportal&p_p_isolated=1&p_v_g_id=12977
	    	System.out.println("Court Calendars added.");
	    	dashBoard.waitForThisManySeconds(1);
		}
		
		try {
			driver.findElement(By.id("portlet_pendingApprovals_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addPendingApprovals();
			System.out.println("Pending Approvals added.");
			dashBoard.waitForThisManySeconds(1);
		}
		
		try {
			driver.findElement(By.id("portlet_recentlyEditedCases_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addRecentlyEditedCases();
			System.out.println("Recently Edited Cases added.");
			dashBoard.waitForThisManySeconds(1);
		}
		
		//using xpath in order to use contains.. RSS Portlets generate random id's
		try {
			driver.findElement(By.xpath("//*[contains(@id, 'p_p_id_39_')]"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addRSSPortal();
			System.out.println("RSS Portal added.");
			dashBoard.waitForThisManySeconds(1);
			
		}
		
		try {
			driver.findElement(By.id("portlet_savedSearches_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addSavedSearchedPortal();
			System.out.println("Saved Searches Portal added.");
			dashBoard.waitForThisManySeconds(1);
		}
		
		try {
			driver.findElement(By.id("portlet_weekAtAGlance_WAR_lbportal"));
		} 
		catch (NoSuchElementException e){
			dashBoard.addWeekAtGlance();
			System.out.println("Week at a Glance added.\n");
			dashBoard.waitForThisManySeconds(1);
		}
		
//		for(int i=0; i<5; i++){
//			
//			driver.navigate().refresh(); 
//			wait(1);
//		}
	}
		@After             
	    public void ending() {
			System.out.println("Script completed successfully. Closing browser.");
			
			logoutPage.logout();
	            try{
				driver.close();
	              Thread.sleep(3000);
	               }
	           catch(Exception b){
	              b.getMessage();
	               }
	     }
		
	//	logoutPage.logout();
		
		
		// driver.manage().deleteAllCookies();
//	     driver.close();
	    // driver.quit();
	//}
	
//	@SuppressWarnings("deprecation")
//	private HttpResponse sendPost(String url, String json) throws Exception{
//		
//		client = new DefaultHttpClient();
//		
//		HttpPost request = new HttpPost(url);
//		
//		StringEntity entity = new StringEntity(json);
//		entity.setContentType("application/json");
//		
//		
//		request.addHeader("CSRF_HEADER", crsf);
//		
//		
//	}
}
